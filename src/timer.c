#include <stdlib.h>

#include "timer.h"

#define CHECK_NULL(timer, ret) \
  if (NULL == timer)           \
  return ret



/******************** Definition of Private Structs ********************/

struct Timer {
  // The clock time when the timer started
  Uint32 startTicks;

  // THe ticks stored when the timer was paused
  Uint32 pausedTicks;

  bool hasStarted;

  bool isPaused;
};



/******************** Definition of Public Functions ********************/

Timer
Timer_create(void) {
  Timer timer = (Timer) calloc(1, sizeof(struct Timer));
  return timer;
}

void
Timer_destroy(Timer timer) {
  CHECK_NULL(timer, );
  free(timer);
}

Uint32
Timer_getTicks(Timer timer) {
  CHECK_NULL(timer, 0);

  if (!timer->hasStarted)
    return 0;

  if (timer->isPaused)
    return timer->pausedTicks;

  return SDL_GetTicks() - timer->startTicks;
}

bool
Timer_hasStarted(Timer timer) {
  CHECK_NULL(timer, false);
  return timer->hasStarted;
}

bool
Timer_isPaused(Timer timer) {
  CHECK_NULL(timer, false);
  return timer->isPaused;
}

void
Timer_pause(Timer timer) {
  CHECK_NULL(timer, );

  if (timer->isPaused || !timer->hasStarted)
    return;

  timer->isPaused = true;
  timer->pausedTicks = SDL_GetTicks() - timer->startTicks;
  timer->startTicks = 0;
}

void
Timer_restart(Timer timer) {
  timer->hasStarted = true;
  timer->isPaused = false;
  timer->startTicks = SDL_GetTicks();
  timer->pausedTicks = 0;
}

void
Timer_resume(Timer timer) {
  CHECK_NULL(timer, );

  if (!timer->isPaused || !timer->hasStarted)
    return;

  timer->isPaused = false;
  timer->startTicks = SDL_GetTicks() - timer->pausedTicks;
  timer->pausedTicks = 0;
}

void
Timer_start(Timer timer) {
  CHECK_NULL(timer, );

  if (timer->hasStarted)
    return;

  Timer_restart(timer);
}

void
Timer_stop(Timer timer) {
  CHECK_NULL(timer, );

  timer->hasStarted = false;
  timer->isPaused = false;
  timer->startTicks = 0;
  timer->pausedTicks = 0;
}