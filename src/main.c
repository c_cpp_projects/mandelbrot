#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#include <inttypes.h>

#include <SDL.h>

#include "simplog.h"
#include "simplog_global.h"

#include "constants.h"
#include "timer.h"

// Starts up SDL and creates window
bool
myInit();

// Loads media
bool
myLoadMedia();

// Frees media and shuts down SDL
void
myClose();

#if (0 != SIMPLOG_GLOBAL_LOG_ENABLE)
void
onLogEvent(const Simplog_Event* pEvent, const char* msg, void* pUserData);
#endif

// The window we'll be rendering to
SDL_Window* gWindow = NULL;

// The window renderer
SDL_Renderer* gRenderer = NULL;

int
main() {
  LOG_CREATE(((Simplog_Config){
               .level = SIMPLOG_LEVEL_VERBOSE,
               .pAllocator = NULL,
               .eventCb = onLogEvent,
               .flushCb = NULL,
             }),
             NULL);

  // Start up SDL and create window
  if (!myInit()) {
    printf("Failed to initialize!\n");
    return 1;
  }

  // Load media
  if (!myLoadMedia()) {
    printf("Failed to load media!\n");
    return 2;
  }

  // Main loop flag
  bool quit = false;

  // Event handler
  SDL_Event e;

  // The fps cap timer
  Timer capTimer = Timer_create();
  Uint32 frameTicks;

  // The application timer
  Timer fpsTimer = Timer_create();

  // Start counting fps
  Uint32 countedFrames = 0;
  Timer_start(fpsTimer);
  float avgFPS = 0;

  // Game loop
  while (!quit) {
    // Start cap time
    Timer_restart(capTimer);

    // Handle events on queue
    while (0 != SDL_PollEvent(&e)) {
      switch (e.type) {
        case SDL_QUIT:
          quit = true;
          break;
      }
    }

    // Calculate and correct fps
    avgFPS = (float) countedFrames / ((float) Timer_getTicks(fpsTimer) / 1000.f);
    if (avgFPS > 2000000) {
      avgFPS = 0;
    }
    LOG_V("Game Loop FPS: %.3f", avgFPS);

    // Clear screen
    SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderClear(gRenderer);

    // Do stuff

    // Update screen
    SDL_RenderPresent(gRenderer);

    ++countedFrames;

    // wait if frame finished early
    frameTicks = Timer_getTicks(capTimer);
    if (SCREEN_TICKS_PER_FRAME > frameTicks) {
      SDL_Delay(SCREEN_TICKS_PER_FRAME - frameTicks);
    }
  }

  Timer_destroy(capTimer);
  Timer_destroy(fpsTimer);

  // Free resources and close SDL
  myClose();

  LOG_DESTROY();

  return 0;
}

bool
myInit() {
  // Initialize SDL
  if (0 != SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS)) {
    printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
    return false;
  }

  // Set texture filtering to linear
  if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
    printf("Warning: Linear texture filtering not enabled!");
  }

  // Create window
  gWindow = SDL_CreateWindow(WINDOW_TITLE,
                             SDL_WINDOWPOS_UNDEFINED,
                             SDL_WINDOWPOS_UNDEFINED,
                             SCREEN_WIDTH,
                             SCREEN_HEIGHT,
                             SDL_WINDOW_SHOWN);
  if (NULL == gWindow) {
    printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
    return false;
  }

  // Create renderer for window
  gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_SOFTWARE);
  if (NULL == gRenderer) {
    printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
    return false;
  }

  // Initialize renderer color
  SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

  return true;
}

bool
myLoadMedia() {
  return true;
}

void
myClose() {
  // Destroy renderer
  SDL_DestroyRenderer(gRenderer);
  gRenderer = NULL;

  // Destroy window
  SDL_DestroyWindow(gWindow);
  gWindow = NULL;

  // Quit SDL subsystems
  SDL_Quit();
}

#if (0 != SIMPLOG_GLOBAL_LOG_ENABLE)
void
onLogEvent(const Simplog_Event* pEvent, const char* msg, void* pUserData) {
  char timestr[20];
  strftime(timestr, sizeof(timestr), "%d-%m-%Y %T", localtime(&pEvent->timestamp.tv_sec));
  const long millis = pEvent->timestamp.tv_nsec / 1000000;

  fprintf(stdout,
          "%s/%s.%03ld\n"
          "%s:%d\n"
          //  "Function: %s\n"
          "%s\n\n",
          Simplog_strLevelShortName(pEvent->level),
          timestr,
          millis,
          pEvent->file,
          pEvent->line,
          //  pEvent->function,
          msg);
  // fflush(stdout);
}
#endif