#ifndef CONSTANTS_H
#define CONSTANTS_H

#define WINDOW_TITLE "Mandelbrot Set Visualization"
#define ASSETS_DIR   "../assets"

// Screen dimension constants
#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480

#define SCREEN_FPS             30
#define SCREEN_TICKS_PER_FRAME (1000 / SCREEN_FPS)

#endif  // CONSTANTS_H