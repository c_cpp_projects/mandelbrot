#ifndef TIMER_H
#define TIMER_H

#include <stdbool.h>
#include <stddef.h>

#include <SDL.h>


typedef struct Timer* Timer;



Timer
Timer_create(void);

void
Timer_destroy(Timer timer);

Uint32
Timer_getTicks(Timer timer);

bool
Timer_hasStarted(Timer timer);

bool
Timer_isPaused(Timer timer);

void
Timer_pause(Timer timer);

/**
 * @brief Start or restart this @c Timer .
 */
void
Timer_restart(Timer timer);

void
Timer_resume(Timer timer);

void
Timer_start(Timer timer);

void
Timer_stop(Timer timer);



#endif  // TIMER_H