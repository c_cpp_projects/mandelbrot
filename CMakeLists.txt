cmake_minimum_required(VERSION 3.16)

project(
  mandelbrot
  VERSION 0.0.1
  DESCRIPTION "Mandelbrot set in C with SDL2"
  HOMEPAGE_URL https://gitlab.com/c_cpp_projects/mandelbrot
  LANGUAGES C
)

set(CMAKE_MESSAGE_LOG_LEVEL VERBOSE)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_EXTENSIONS OFF)

set(HOME_BIN_DIR ${CMAKE_HOME_DIRECTORY}/bin)
set(HOME_INC_DIR ${CMAKE_HOME_DIRECTORY}/include)
set(HOME_LIB_DIR ${CMAKE_HOME_DIRECTORY}/lib)
set(HOME_SRC_DIR ${CMAKE_HOME_DIRECTORY}/src)

set(EXECUTABLE ${PROJECT_NAME})

# Find necessary packages
find_package(SDL2 REQUIRED)

# Enable static linking
add_compile_options(
  -static -static-libgcc
)

# Enable all warnings
add_compile_options(
  -Wall -Wextra -Wconversion
)

include_directories(
  ${HOME_INC_DIR}
)

# Add third party libs

function(add_third_party LIB_NAME)
  include_directories(third_party/${LIB_NAME}/include)
  add_subdirectory(third_party/${LIB_NAME})

  # Add all compile definitions
  get_directory_property(
    defs
    DIRECTORY ${CMAKE_SOURCE_DIR}/third_party/${LIB_NAME}
    COMPILE_DEFINITIONS
  )
  
  message(VERBOSE "COMPILE_DEFINITIONS in library ${LIB_NAME}:\n${defs}" )
  add_compile_definitions(${defs})
  
  message(VERBOSE "Added third party library: ${LIB_NAME}")
endfunction()

add_third_party(simplog)



# Add all subdirs
add_subdirectory(src)

# Link with libraries
target_link_libraries(
  ${EXECUTABLE}
  SDL2main SDL2 ${LIB_SIMPLOG}
)